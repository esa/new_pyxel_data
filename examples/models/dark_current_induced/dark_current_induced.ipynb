{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Simulating radiation induced dark current\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Florian Moriousef, Vincent Goiffon, Alexandre Le Roch, Aubin Antonsanti ([ISAE-SUPAERO](https://www.isae-supaero.fr/en/))\n",
    "\n",
    "## Keywords\n",
    "radiation induced dark-current, displacement damage effect, CMOS, radiation\n",
    "\n",
    "## Summary\n",
    "This model \"radiation_induced_dark_current\" inside the charge generation model group adds dark current induced by radiation for Silicon CMOS detectors. The input parameters of the model are the ```depletion_volume``` parameter in µm3, ```annealing_time``` in weeks and the ```displacement_dose``` in TeV/g. Shot noise can be included when setting the parameter ```shot_noise``` to True.\n",
    "\n",
    "![Title](images/displacement_damage.jpg)\n",
    "$\\textbf{Figure 1}$ Definition of mean dark current increase induced by displacement damage.\n",
    "\n",
    "\n",
    "![Title](images/damage_induced.jpg)\n",
    "$\\textbf{Figure 2}$ Definition of the displacement damage induced dark current distribution.\n",
    "\n",
    "\n",
    "![Title](images/measurements.jpg)\n",
    "$\\textbf{Figure 3}$ Validation of model with measurements.\n",
    "\n",
    "\n",
    "![Title](images/occurence.jpg)\n",
    "$\\textbf{Figure 4}$ Occurence of displacement damage induced dark current 'events'\n",
    "\n",
    "\n",
    "A more detailed description of the models can be found in **[1]**, **[2]** and the [Radiation Induced Dark Current Model Tutorial](images/2024_03_20_V.Goiffon_DarkCurrentModelPyxel.pdf).\n",
    "\n",
    "### References\n",
    "**[1]**: Alexandre Le Roch, Cédric Virmontois, Philippe Paillet, Jean-Marc Belloir, Serena Rizzolo, Federico Pace, Clémentine Durnez, Pierre Magnan, and Vincent Goiffon. **Radiation-induced leakage current and electric field enhancement in cmos image sensor sense node floating diffusions**. IEEE Transactions on Nuclear Science, 66(3):616–624, 2019. doi:10.1109/TNS.2019.2892645.\n",
    "\n",
    "**[2]**: Jean-Marc Belloir, Vincent Goiffon, Cédric Virmontois, Mélanie Raine, Philippe Paillet, Olivier Duhamel, Marc Gaillardin, Romain Molina, Pierre Magnan, and Olivier Gilard. **Pixel pitch and particle energy influence on the dark current distribution of neutron irradiated cmos image sensors.** Opt. Express, 24(4):4299–4315, Feb 2016. URL: https://opg.optica.org/oe/abstract.cfm?URI=oe-24-4-4299, doi:10.1364/OE.24.004299."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pyxel\n",
    "from pyxel import display_detector\n",
    "\n",
    "pyxel.__version__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "### Exposure mode\n",
    "\n",
    "See induced dark current with ```display_detector``` on pixel array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"dark_current_induced.yaml\")\n",
    "\n",
    "exposure = config.exposure\n",
    "pipeline = config.pipeline\n",
    "detector = config.detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree = pyxel.run_mode(mode=exposure, pipeline=pipeline, detector=detector)\n",
    "\n",
    "data_tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_detector(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "### Observation mode\n",
    "\n",
    "See induced dark current versus temperature for different annealing times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"dark_current_induced_observation.yaml\")\n",
    "\n",
    "observation = config.observation\n",
    "pipeline = config.pipeline\n",
    "detector = config.detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(mode=observation, pipeline=pipeline, detector=detector)\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "dark_current = result[\"/bucket/pixel\"].mean(dim=[\"x\", \"y\"])\n",
    "# dark_current_induced = dark_current.sel(annealing_time=0.1)\n",
    "\n",
    "# Add more attributes\n",
    "dark_current[\"annealing_time\"].attrs = {\n",
    "    \"units\": \"week\",\n",
    "    \"standard_name\": \"Annealing time\",\n",
    "}\n",
    "dark_current[\"temperature\"].attrs = {\"units\": \"K\", \"standard_name\": \"Temperature\"}\n",
    "dark_current.attrs = {\"units\": \"e⁻/pixel/s\", \"standard_name\": \"Dark current\"}\n",
    "\n",
    "dark_current"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "dark_current.plot(hue=\"annealing_time\")\n",
    "plt.title(\"Dark current vs. temperature for different annealing times\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "import hvplot.xarray  # noqa\n",
    "\n",
    "result[\"/bucket/pixel\"].plot(col=\"temperature\", row=\"annealing_time\", robust=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/bucket/pixel\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "import colorcet as cc\n",
    "\n",
    "(\n",
    "    result[\"/bucket/pixel\"].hvplot.image(\n",
    "        aspect=1.0,\n",
    "        cmap=cc.fire,\n",
    "        flip_yaxis=True,\n",
    "        groupby=[\"time\", \"temperature\", \"annealing_time\"],\n",
    "    )\n",
    "    + result[\"/bucket/pixel\"].hvplot.hist(\n",
    "        aspect=1.0, bins=10, logy=True, ylim=(1, 1000)\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "import numpy as np\n",
    "\n",
    "(\n",
    "    hv.Curve(\n",
    "        (\n",
    "            np.logspace(0, 10, num=100),\n",
    "            np.logspace(-16, -16, num=100),\n",
    "        )\n",
    "    ).opts(\n",
    "        logx=True,\n",
    "        logy=True,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
