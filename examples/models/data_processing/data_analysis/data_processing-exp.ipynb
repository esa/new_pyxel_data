{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# [Data processing models:](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/data_processing_models.html) Exposure mode\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Constanze Seibert\n",
    "\n",
    "## Keywords\n",
    "data processing, flat field simulation, exposure mode\n",
    "\n",
    "## Learning Goals\n",
    "* use of [xarray.DataTree](https://xarray-datatree.readthedocs.io/en/latest/)\n",
    "* use of [Data processing models](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/data_processing_models.html) in exposure mode\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`exposure_mode`  | Necessary | Background |\n",
    "| [Xarray DataTree](https://xarray-datatree.readthedocs.io/en/latest/quick-overview.html) | Helpful | |\n",
    "| [Xarray](https://xarray.dev) | Helpful | |\n",
    "| [matplotlib](https://matplotlib.org) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "In the development towards Pyxel version 2.0, we introduced two new model groups. The **Data processing** model group at the end of the pipeline aims to do some Data processing inside of Pyxel. The image below shows the planned architecture for version 2.0.\n",
    "\n",
    "<img style=\"float: right;\" src=\"images/pipeline.png\" width=\"350\">\n",
    "\n",
    "This notebook shows the usage of several basic operation models in the model group **Data processing**, namely ```statistics```, ```mean_variance```, ```linear_regression``` and ```signal_to_noise_ratio``` in [exposure mode](https://esa.gitlab.io/pyxel/doc/latest/background/running_modes/exposure_mode.html). \n",
    "Another model, which is available since version 1.8 is the Source extractor model ```extract_roi_to_xarray```, where a seperate notebook is available [here](https://esa.gitlab.io/pyxel-data/examples/models/data_processing/source_extractor/SEP_exposure.html).\n",
    "\n",
    "In this example we are simulating flat field images with increasing integration times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import packages\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pyxel\n",
    "import xarray as xr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load configuration file\n",
    "config = pyxel.load(\"data_example.yaml\")\n",
    "\n",
    "exposure = config.exposure\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "Since version 1.8, Pyxel function ```run_mode()``` returns a [xarray.DataTree](https://xarray-datatree.readthedocs.io/en/latest/) and contains the output of the (Processed) Data from each model used in the YAML configuration file. \n",
    "Datatree is a prototype implementation of a tree-like hierarchical data structure for xarray.\n",
    "\n",
    "\n",
    "### Result retrieved with ```run_mode()``` will show DataTree structure\n",
    "**Data group** contains the groups for each Data processing model used in the YAML configuration file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=exposure,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    "    with_inherited_coords=True,\n",
    ")\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.set_options(display_style=\"text\")  # text or html\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "Currently, we get the same output using the result retrieved from the ```run_mode()``` and the ```detector.data```. In a future Pyxel version the old usage will maybe deprecated.\n",
    "\n",
    "### Statistics model example\n",
    "\n",
    "Another way to access the **(Processed) Data** of a Data processing model is with \n",
    "```detector.data.<model_name>```, e.g. ```detector.data.statistics```. \n",
    "The model ```statistics``` can be used to do simple statistics computations,\n",
    "giving the  **variance, mean, min, max** and **count** of the data buckets\n",
    "*photon, pixel, signal* and *image* of the detector along the time.\n",
    "The calculated statistics can then be accessed via ```detector.data.statistics```.\n",
    "We can also access each bucket and calculation like a path of the DataTree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data[\"/statistics/pixel/var\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "The same is possible with the dot annotation, but **be careful** since *.var* is a function itself, so it will calculate again the variance and not give just the output of the calculated variance!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "result.data.statistics.pixel.var"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data[\"/statistics/pixel/mean\"].plot()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "Beware, the readout-time does not necessarily corresponds to the exposure or integration duration depending on the selected readout mode.\n",
    "\n",
    "With ```non_destructive: true``` we are having **non-destructive** readout mode. While reading pyxel keeps integrating the light.\n",
    "\n",
    "With ```non_destructive: false``` we are having **destructive** readout mode. (Typical usage for monolytic CCDs)\n",
    "\n",
    "The integration duration corresponds to $readouttime_\\left(i+1\\right)-readouttime_\\left(i\\right)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "integration_times = [\n",
    "    0.02,\n",
    "    0.04,\n",
    "    0.06,\n",
    "    0.08,\n",
    "    0.1,\n",
    "    0.2,\n",
    "    0.3,\n",
    "    0.4,\n",
    "    0.5,\n",
    "    0.6,\n",
    "    0.7,\n",
    "    0.8,\n",
    "    0.9,\n",
    "    1.0,\n",
    "    1.1,\n",
    "    1.2,\n",
    "    1.3,\n",
    "    1.4,\n",
    "    1.5,\n",
    "    1.6,\n",
    "    1.7,\n",
    "    1.8,\n",
    "    1.9,\n",
    "    2.0,\n",
    "    2.1,\n",
    "    2.2,\n",
    "    2.3,\n",
    "    2.4,\n",
    "    2.5,\n",
    "    2.6,\n",
    "    2.7,\n",
    "    2.8,\n",
    "    2.9,\n",
    "    3.0,\n",
    "    3.1,\n",
    "    3.2,\n",
    "    3.3,\n",
    "    3.4,\n",
    "    3.5,\n",
    "    3.6,\n",
    "    3.7,\n",
    "    3.8,\n",
    "    3.9,\n",
    "    4.0,\n",
    "    4.1,\n",
    "    4.2,\n",
    "    4.3,\n",
    "    4.4,\n",
    "    4.5,\n",
    "    4.6,\n",
    "    4.7,\n",
    "    4.8,\n",
    "    4.9,\n",
    "    5.0,\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(detector.data[\"/statistics/pixel/mean\"], integration_times)\n",
    "plt.xlabel(\"Integration time [s]\")\n",
    "plt.ylabel(\"Pixel mean\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "### Signal-to-noise ratio example\n",
    "The model ```signal_to_noise_ratio``` computes the mean and the variance of the input array and calculates the ratio of that for each time step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data[\"/snr/signal\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "### Mean-variance model example\n",
    "The model ```mean_variance``` was developed to do a quick and easy Photon-Transfer curve (PTC) analysis, where you need the calculated mean and variance of the image. It computes a mean-variance 1D array that shows relationship between the mean signal of a detector and its variance for a selected data_structure (default is \"image\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_variance = detector.data[\"/mean_variance/image/variance\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_variance.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "### Linear regression model example\n",
    "The linear regression model was developed for quick and easy linear regression analysis of the simulation. It computes a linear regression along readout time for a selected data_structure (default is \"image\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/data/linear_regression\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "Plotting the intercept of the pixel shows the illumination level per pixel computed along the time. Illumination level is 5000 e-/s (see YAML config file).\n",
    "\n",
    "When including more noise with enabling more models, the intercept values go down respectively:\n",
    "\n",
    "- Simple dark current with dark_rate: 20 e-/pixel/second -> reduces intercept by factor of 20.\n",
    "- Loading a PRNU file -> reduces intercept "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/data/linear_regression/pixel/intercept\"].plot(robust=True, yincrease=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "prnu = np.load(\"data/noise.npy\")\n",
    "plt.figure(100000)\n",
    "np.shape(prnu)\n",
    "plt.imshow(prnu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
