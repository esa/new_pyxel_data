{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Scene generation and projection\n",
    "\n",
    "## Author\n",
    "2023&mdash;Constanze Seibert & Frederic Lemmel\n",
    "\n",
    "## Learning goals\n",
    "In this notebook you will learn how to use two new models \n",
    "- [```load_star_map```](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/scene_generation_models.html#load-star-map) to generate a scene \n",
    "\n",
    "Generates a scene from scopesim Source object loading objects from the GAIA catalog for given coordinates and FOV.\n",
    "\n",
    "and \n",
    "- [```simple_aperture```](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/photon_collection_models.html#simple-aperture) to project this scene onto your detector. \n",
    "\n",
    "Converts scene to photon with given aperture. First an xarray Dataset will be extracted from the Scene for a selected wavelength band, where the flux of the objects will be integrated along the wavelength band. This integrated flux in photon/(s cm2) is converted to photon/(s pixel). Finally, the objects are projected onto detector, while converting the object coordinates from arcsec to detector coordinates (pixel).\n",
    "\n",
    "\n",
    "Both models are are implemented in Pyxel since version 1.11.\n",
    "\n",
    "## Keywords\n",
    "scene generator, star map, flux conversion\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in exposure mode and generate a scene from the Gaia catalog with a virtual telescope pointing towards the Pleiades with a FOV diameter of $0.5^{\\circ}$. We visualise some selected spectra and the scene in arcsec. Then this scene is projected onto the detector using a simple aperture and a PSF model."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "#### Loading packages "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.colors as colors\n",
    "import matplotlib.pyplot as plt\n",
    "import pyxel\n",
    "import xarray as xr"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "#### Load configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cfg = pyxel.load(\"tutorial-scene.yaml\")\n",
    "\n",
    "detector = cfg.detector"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "#### Loading result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(mode=cfg.exposure, detector=detector, pipeline=cfg.pipeline)\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "### Get Scene from DataTree"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "Info: Detector object contains only last exposure (time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree = detector.scene.data\n",
    "\n",
    "data_tree[\"/list/0\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result.scene\n",
    "result[\"scene/list/0\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Turn DataTree to Dataset\n",
    "For now needed, because plotting not available in DataTree yet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "ds = data_tree[\"/list/0\"].to_dataset()\n",
    "\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "### Visualize spectra of selected stars"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"flux\"].isel(ref=slice(None, 9)).plot.line(col=\"ref\", sharey=False, col_wrap=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"flux\"].isel(ref=100).plot.line()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "### Visualize scene with magnitude as intensity scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "ds.plot.scatter(x=\"x\", y=\"y\", hue=\"weight\", marker=\"o\", size=8)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "## Display detector "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "The new features in the function ```display_detector``` will be part of the next release 1.12 (coming soon)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "Alternative to display_detector to visualize photon and image array in logarithmic scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.DataArray(result.photon).plot(\n",
    "    norm=colors.LogNorm(\n",
    "        vmin=0.1,  # 1\n",
    "        vmax=result.photon.max(),\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.DataArray(result.image).plot(\n",
    "    norm=colors.LogNorm(\n",
    "        vmin=1,  # 0.1\n",
    "        vmax=result.image.max(),\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "### TO DOs:\n",
    "We have a first nice working version, but still want to improve the models.\n",
    "See https://gitlab.com/esa/pyxel/-/issues/668."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
