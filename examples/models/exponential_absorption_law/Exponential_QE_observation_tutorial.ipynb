{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Charge Generation from detector geometry - Observation mode\n",
    "\n",
    "## Author\n",
    "2024&mdash;Vincent Affatato & Frederic Lemmel\n",
    "\n",
    "## Learning goals\n",
    "In this notebook you will learn how to use the new module exponential_qe (https://gitlab.com/esa/pyxel/-/blob/master/pyxel/models/charge_generation/exponential_qe.py) to generate charges over a detector array using an exponential absorption law.\n",
    "\n",
    "The models is implemented in Pyxel since version 2.8.\n",
    "\n",
    "## Keywords\n",
    "charge generation, quantum efficiency, exponential absoprtion law\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in observation mode and generate a scene from a predefined image. \n",
    "\n",
    "The photons, collected from the light sources, are then distributed over the pixel array and converted into electrons according to an empirical absorption law for silicon (Janesick, 2001). This equation encompasses some construction parameters of the detector, like the epitaxial and the poly layers thickness (the latter in case of front illuminated sensors), and intrinsic qualities like the Charge Collection Efficiency, the reflectivity, and the absorptivity. \n",
    "\n",
    "To address these two final ones, the operative wavelength (for both) and temperature (for the absorptivity alone) of the detector must be handled. In this example we will see the effect of changing these variables. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "#### Loading packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import hvplot.xarray\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "#### Using Client to keep track of the progresses"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "from distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "#### Loading configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "cfg = pyxel.load(\"observation-mono.yaml\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "#### Loading result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    detector=cfg.detector,\n",
    "    mode=cfg.running_mode,\n",
    "    pipeline=cfg.pipeline,\n",
    "    with_inherited_coords=True,\n",
    ")\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "#### Showing photons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/bucket/photon\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "#### Showing charges for the first integration time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/bucket/charge\"].isel(time=-1)  # .plot(robust=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "#### Plotting global variations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "# result[\"/bucket/image\"].hvplot(\n",
    "#     by=['x_epi'],\n",
    "#     colorbar=True,\n",
    "#     groupby=['time', 'x', 'y'],\n",
    "#     kind='image',\n",
    "#     x='temperature',\n",
    "#     y='default_wavelength',\n",
    "#     legend='bottom_right',\n",
    "#     widget_location='bottom',\n",
    "# )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "### Obtaining interactive plots"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "#### Display final image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/bucket/image\"].hvplot.image(\n",
    "    y=\"y\", x=\"x\", aspect=1.0, flip_yaxis=True, cmap=\"viridis\", clim=(0, 30000)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "#### Display mean values data for images"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "values = result[\"/bucket/image\"].mean(dim=[\"y\", \"x\"])\n",
    "\n",
    "values"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "You can get all the mean values by running the following command:\n",
    "\n",
    "```python\n",
    "values.load()\n",
    "values.to_netcdf('values.nc')\n",
    "```\n",
    "\n",
    "but it will take more than 5 minutes."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "#### Saving images to a net file"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "#### Importing packages and opening the net file as a data array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import xarray as xr\n",
    "\n",
    "values = xr.open_dataarray(\"values.nc\")\n",
    "\n",
    "values.load()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "#### Plotting collected photon per each image at each different wavelength"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "values.hvplot(\n",
    "    x=\"time\",\n",
    "    by=\"default_wavelength\",\n",
    "    groupby=[\"x_epi\", \"temperature\"],  # Group by both 'x_epi' and 'temperature'\n",
    "    clim=(0, 30000),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "#### Example with a few wavelengths"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select every 40 index from 0 to 199\n",
    "step = 40\n",
    "indices = range(0, 200, step)  # Adjust the range as needed\n",
    "\n",
    "# Use isel to select the indices\n",
    "filtered_data = values.isel(default_wavelength=indices)\n",
    "\n",
    "# Plot using hvplot\n",
    "filtered_data.hvplot(\n",
    "    x=\"time\",\n",
    "    by=\"default_wavelength\",\n",
    "    groupby=[\"x_epi\", \"temperature\"],\n",
    "    clim=(0, 30000),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "#### Plotting the absorption performance of the detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "values.hvplot.bar(x=\"default_wavelength\", y=\"image\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "# qe_loaded = xr.open_dataset(notebooks/qe_interpolated_quantum_efficiency.nc)\n",
    "# display(qe_loaded)"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
