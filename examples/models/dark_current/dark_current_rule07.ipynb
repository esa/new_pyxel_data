{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Dark current vs temperature for MCT detectors\n",
    "\n",
    "##  Author\n",
    "2022&mdash;Constanze Seibert\n",
    "\n",
    "## Keywords\n",
    "dark current, observation mode, temperature, MCT detector\n",
    "\n",
    "## Learning Goals\n",
    "In this notebook we use Pyxel in observation mode and visualize the results of the dark current versus temperature for rule07.\n",
    "If one or more values for the fixed pattern noise factor are given it will include the Dark Signal Non Uniformity (DSNU).\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`observation_product_mode`  | Necessary | Background |\n",
    "| [Dask/Distributed](https://distributed.dask.org/en/stable/quickstart.html) | Helpful | |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "| [matplotlib](https://matplotlib.org) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in observation mode by changing the following parameters:\n",
    "1. Temperature from detector operating temperature up to room temperature in K\n",
    "2. Dark current spatial noise factor is typically between 0.1 and 0.4.\n",
    "\n",
    "Check also the [documentation](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#dark-current).\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Tip:</b> Open the <i>observation_rule07.yaml</i> file in parallel for a better overview.</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "## Loading the configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a configuration class from the yaml file\n",
    "config = pyxel.load(\"observation_rule07.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.detector  # class CMOS\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Running the pipelines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "## Plotting the outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "The plot will show the dark current model rule 07 used in Pyxel against the temperature for different fixed pattern noise (FPN) values. The dark current figure of merit at 300 K is 0.01 nA/cm^2.\n",
    "Higher FPN values result in higher dark current values, while all calculations converge for temperatures higher than 180 K.\n",
    "\n",
    "You can set temporal noise to **true** in the yaml file, run everything again and see the difference in the plot, if temporal noise is included. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "dark_current = result.pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    "temperature = result.temperature.to_numpy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(0, len(result.spatial_noise_factor)):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"FPN factor: {result.spatial_noise_factor.values[i]}\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.xlim([145, 200])\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "## Plotting image as function of readout time, temperature and figure of merit\n",
    "\n",
    "Library `holoviews` can be used to quickly plot data stored inside `xarray` datasets.\n",
    "Here with the `bokeh` backend, `matplotlib` is also supported.\n",
    "\n",
    "You can use the toolbar to change temperature and figure of merit to see the influence on the detector.\n",
    "\n",
    "Try and change the readout time in the configuration file, reload the configuration and run the pipeline again. Then display the results and see the influence of the readout time.\n",
    "\n",
    "Longer readout times result in higher dark current and saturation is reached at lower temperatures already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "out_1a = hv.Dataset(result[\"image\"])\n",
    "plot_1a = out_1a.to(hv.Image, [\"x\", \"y\"], dynamic=True)\n",
    "plot_1a.opts(opts.Image(aspect=1, cmap=\"gray\", tools=[\"hover\"]))"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc-autonumbering": true,
  "toc-showcode": true,
  "toc-showmarkdowntxt": true
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
