# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example yaml configuration file                             #
# multi wavelength capability                                 #
# Created by C. Nghiem                                        #
# ########################################################### #


# yaml-language-server: $schema=https://esa.gitlab.io/pyxel/doc/latest/pyxel_schema.json
exposure:

  readout:
    times: [6000] # in s
    non_destructive:  false
    
  outputs:
    output_folder: "output"
    save_data_to_file:
      - detector.image.array: ['fits']

ccd_detector:

  geometry:

    row: 500
    col: 500    
    total_thickness: 40.    # um dimension 49x49 mm?
    pixel_vert_size: 12.    # um
    pixel_horz_size: 12.    # um
    pixel_scale: 1.38       # arcsec/pixel

  environment:
    temperature: 150  # K 
    wavelength: 600   # nm

  characteristics:
    quantum_efficiency: 0.9          
    charge_to_volt_conversion: 1.0e-6 
    pre_amplification: 4.          
    adc_voltage_range: [0.0, 10.0]    
    adc_bit_resolution: 16
    full_well_capacity: 175000 #e-   


pipeline:
  # scene -> scene:
  scene_generation:
    - name: scene_generator
      func: pyxel.models.scene_generation.load_star_map
      enabled: true
      arguments:
        right_ascension: 56.75 # deg
        declination: 24.1167 # deg
        fov_radius: 0.5 # deg
      
  # scene -> photon
  photon_collection:
    # scene -> 2D photon
    - name : simple_collection
      func: pyxel.models.photon_collection.simple_collection
      enabled: true
      arguments:
        aperture: 126.70e-3 #m
        filter_band: [600, 650] #nm
        resolution: 10 #nm
        integrate_wavelength: true

    - name: load_psf
      func: pyxel.models.photon_collection.load_psf
      enabled: true
      arguments:
        filename: "data/psf_VIS.fits"
        normalize_kernel: true  # optional


    # 2D photon -> 2D photon
    - name: optical_psf
      func: pyxel.models.photon_collection.optical_psf
      enabled: true
      arguments:
        fov_arcsec: 5 # FOV in arcseconds
        pixel_scale: 0.01 #arcsec/pixel
        wavelength: 630 # wavelength in meters
        optical_system:
          - item: CircularAperture
            radius: 3.0

        # fov_arcsec: 13.8    # FOV in arcseconds
        # # wavelength: 600 #nm
        # apply_jitter: false
        # optical_system:
        #   - item: CircularAperture
        #     radius: 0.074       # radius in meters
        #   - item: SecondaryObscuration
        #     secondary_radius: 0.038  # m
        #     n_supports: 3
        #     support_width: 0.0005 #m

  # photon -> charge
  charge_generation:
  # 2D photon -> 2d charge
    - name: simple_conversion
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true
      
    - name: full_well
      func: pyxel.models.charge_collection.simple_full_well
      enabled: true
      
    - name: fixed_pattern_noise
      func: pyxel.models.charge_collection.fixed_pattern_noise
      enabled: true
      arguments:
        fixed_pattern_noise_factor: 0.014
        seed: 12345


  # pixel -> pixel
  charge_transfer:

  # pixel -> signal
  charge_measurement:

    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true
        
    - name: output_noise
      func: pyxel.models.charge_measurement.output_node_noise
      enabled: true
      arguments:
        std_deviation: 5.4585266113281245e-05
    
  # signal -> image
  readout_electronics:
    - name: simple_amplifier
      func: pyxel.models.readout_electronics.simple_amplifier
      enabled: true
    - name: simple_adc
      func: pyxel.models.readout_electronics.simple_adc
      enabled: true
