{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Multi-wavelength capability \n",
    "\n",
    "## Authors\n",
    "2024&mdash;Constanze Nghiem & Frederic Lemmel\n",
    "\n",
    "## Learning goals\n",
    "In this notebook you will learn how to use Pyxel with multi-wavelength capability. \n",
    "\n",
    "Since version 2.0, Pyxel has the capability to support multi-wavelength models. These models, along with their respective groups, are visually distinguished by color in the accompanying image. Integration of multi-wavelength photons occurs either in the photon collection or in the charge generation model group, ensuring that they are consolidated across the specified wavelength range.\n",
    "\n",
    "<img style=\"float: middle;\" src=\"data/architecture.png\" width=\"1600\">\n",
    "\n",
    "Furthermore, you will learn how to use the wavelength capabable models within the pipeline.\n",
    "- [```load_star_map```](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/scene_generation_models.html#load-star-map) is used to generate a scene.\n",
    "\n",
    "- [```simple_collection```](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/photon_collection_models.html#simple-collection) is used to project the scene onto the detector. If ``integrate_wavelength`` is false, a multi-wavelength photon object is created.\n",
    "\n",
    "- [```optical_psf```](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/photon_collection_models.html#physical-optics-propagation-in-python-poppy) is used to generate a PSF and convolute it with the photon array. Multi-wavelength or monochromatic photons are possible.\n",
    "\n",
    "- [```load_wavelength_psf```](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/photon_collection_models.html#load-multi-wavelength-psf) is used to load a multi-wavelength PSF from a file and convolute it with the photon array. The same model exists already for monochromatic photons and is called ```load_psf``.\n",
    "\n",
    "- [```apply_qe_curve```](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#apply-qe-curve) is used to apply a wavelength dependent QE to the multi-wavelength photon.\n",
    "\n",
    "- [```conversion_with_3d_qe_map```](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#conversion-with-3d-qe-map) is used to apply a multi-wavelength QE map to the multi-wavelength photon.\n",
    "\n",
    "\n",
    "The improved function [```display_detector```](https://esa.gitlab.io/pyxel/doc/latest/references/api/notebook.html#pyxel.display_detector) and the newly added [```display_scene```](https://esa.gitlab.io/pyxel/doc/latest/references/api/notebook.html#pyxel.display_scene) are used in this notebook to visualize the scene and the other detector buckets.\n",
    "\n",
    "\n",
    "## Keywords\n",
    "scene generator, mulit-wavelength, 3D photon\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in exposure mode and use models with multi-wavelength capability in the pipeline.\n",
    "\n",
    "First, you will use a ``YAML`` file that is using the monochromatic Pyxel as the integration of the multi-wavelength photons is happening at the beginning of the ``photon_collection`` model group.\n",
    "\n",
    "Seconnd, you will use the multi-wavelength capabilties and create a multi-wavelength photon bucket, while the integration of the multi-wavelength photons is happening at the beginning of the ``charge_generation`` model group right before converting photon to charge. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "### Loading packages "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "## Scene -> monochromatic Photon\n",
    "Snippet from ```YAML``` configuration file: \n",
    "\n",
    "```yaml\n",
    "ccd_detector:\n",
    "\n",
    "  geometry:\n",
    "\n",
    "    row: 500\n",
    "    col: 500    \n",
    "    total_thickness: 40.    # um dimension 49x49 mm?\n",
    "    pixel_vert_size: 12.    # um\n",
    "    pixel_horz_size: 12.    # um\n",
    "    pixel_scale: 1.38       # arcsec/pixel\n",
    "\n",
    "  environment:\n",
    "    temperature: 150  # K \n",
    "    wavelength: 600   # nm\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "#### Load configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cfg = pyxel.load(\"wavelength_example.yaml\")\n",
    "detector = cfg.detector"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "#### Loading result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=cfg.exposure,\n",
    "    detector=detector,\n",
    "    pipeline=cfg.pipeline,\n",
    "    debug=True,  # Export all intermediate steps into \"result['/intermediate']\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### Display scene with detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_scene(detector);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(\n",
    "    detector,\n",
    "    new_display=True,  # Enable new display mode. This mode is provisional and may change.\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "## Scene -> multi-wavelength photon\n",
    "Snippet from ```YAML``` configuration file: \n",
    "\n",
    "```yaml\n",
    "ccd_detector:\n",
    "\n",
    "  geometry:\n",
    "\n",
    "    row: 500\n",
    "    col: 500    \n",
    "    total_thickness: 40.    # um dimension 49x49 mm?\n",
    "    pixel_vert_size: 12.    # um\n",
    "    pixel_horz_size: 12.    # um\n",
    "    pixel_scale: 1.38       # arcsec/pixel\n",
    "\n",
    "  environment:\n",
    "    temperature: 150  # K \n",
    "    wavelength: \n",
    "      cut_on: 600     # nm\n",
    "      cut_off: 650    # nm\n",
    "      resolution: 10  # nm\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "cfg_multi = pyxel.load(\"multi_wavelength_example.yaml\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "result_multi = pyxel.run_mode(\n",
    "    mode=cfg_multi.exposure,\n",
    "    detector=cfg_multi.detector,\n",
    "    pipeline=cfg_multi.pipeline,\n",
    "    with_inherited_coords=True,\n",
    "    debug=True,\n",
    ")\n",
    "\n",
    "result_multi[\"/bucket\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "result_multi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "import hvplot.xarray  # Enable 'hvplot' in 'xarray'\n",
    "\n",
    "result_multi[\"/bucket/photon\"].hvplot.image(\n",
    "    groupby=[\"time\", \"wavelength\"],\n",
    "    aspect=1.0,\n",
    "    cmap=\"viridis\",\n",
    "    cnorm=\"log\",\n",
    "    flip_yaxis=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(cfg_multi.detector, new_display=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
