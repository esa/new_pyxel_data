{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Charge transfer ineffiency"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "## Author\n",
    "2022&mdash;Bradley Kelman\n",
    "\n",
    "## Learning Goals\n",
    "* use of calibration mode\n",
    "* simulation of Charge-Transfer Inefficiency\n",
    "\n",
    "## Keywords\n",
    "Charge-Transfer Inefficiency (CTI), charge injection\n",
    "\n",
    "## Summary\n",
    "This notebook gives an example of the use for calibration mode, it will explore the effect of charge transfer inefficiency (CTI) on 1D charge injection profiles. It will attempt to find the correct trap parameters (trap release timescales and trap density) of a damaged profile that contains CTI. The input (the profiles the Pyxel uses to simulate the CTI effect) will be CTI free charge profiles and the target data (the profiles Pyxel uses for comparison to obtain a fitness value) will be the CTI damaged profiles.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "## Calibration mode\n",
    "\n",
    "Calibration mode in Pyxel is designed for finding and optimising given arguments of a model. The calibration mode itself takes in two images, the first is an input profile which is free of the detector effect that needs to be simulated and second is the target image, this is an image that contains the desired detector effect. The calibration mode will simulate the desired detector effect multiple times on the input profile in an attampt to get its resultant simulated image to resemble the target image as close as possible. An image of how the simulation works is seen below. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "![Calibration](images/calibration.png) Figure 1 - Overview of Pyxels calibrations mode "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Initialization"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "Import required packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dask\n",
    "import numpy as np\n",
    "import pyxel\n",
    "from pyxel.notebook import (\n",
    "    champion_heatmap,\n",
    "    display_evolution,\n",
    "    display_simulated,\n",
    "    optimal_parameters,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "Check Pyxel version "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display current version of Pyxel\n",
    "\n",
    "print(\"Pyxel version:\", pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### Create a ``Dask`` cluster\n",
    "\n",
    "Used for the parallisation of calibration mode"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a 'local' Cluster\n",
    "from distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "Defining the input and target filenames to be used in the calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "input_filenames = [\n",
    "    \"profiles/input/r_G0_0.txt\",\n",
    "    \"profiles/input/r_G0_10.txt\",\n",
    "    \"profiles/input/r_G0_20.txt\",\n",
    "    \"profiles/input/r_G0_30.txt\",\n",
    "    \"profiles/input/r_G0_40.txt\",\n",
    "    \"profiles/input/r_G0_50.txt\",\n",
    "    \"profiles/input/r_G0_60.txt\",\n",
    "    \"profiles/input/r_G0_70.txt\",\n",
    "    \"profiles/input/r_G0_80.txt\",\n",
    "    \"profiles/input/r_G0_90.txt\",\n",
    "    \"profiles/input/r_G0_100.txt\",\n",
    "    \"profiles/input/r_G0_110.txt\",\n",
    "    \"profiles/input/r_G0_120.txt\",\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "target_filenames = [\n",
    "    \"profiles/target/CTIr_G0_0.txt\",\n",
    "    \"profiles/target/CTIr_G0_10.txt\",\n",
    "    \"profiles/target/CTIr_G0_20.txt\",\n",
    "    \"profiles/target/CTIr_G0_40.txt\",\n",
    "    \"profiles/target/CTIr_G0_30.txt\",\n",
    "    \"profiles/target/CTIr_G0_50.txt\",\n",
    "    \"profiles/target/CTIr_G0_60.txt\",\n",
    "    \"profiles/target/CTIr_G0_70.txt\",\n",
    "    \"profiles/target/CTIr_G0_80.txt\",\n",
    "    \"profiles/target/CTIr_G0_90.txt\",\n",
    "    \"profiles/target/CTIr_G0_100.txt\",\n",
    "    \"profiles/target/CTIr_G0_110.txt\",\n",
    "    \"profiles/target/CTIr_G0_120.txt\",\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Creating target data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "### Explanation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "Pyxels calibration mode can optimise given argumentes using multiple input and target images (as long as both contain the same number of images), this has uses if there are multiple images with different exposure times or as the case in this example there are multiple signal values on the same detector. The fitness values obtained in this case will be based upon the sum of the residuals from each image given."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "### Observation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "The best way to produce multiple charge injection profiles from out input profiles is to use Pyxels observation mode which will simulated the addition of CTI to each charge profile sequentially. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"yaml_files/cdm_observation.yaml\")\n",
    "\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline\n",
    "observation = config.observation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "result = pyxel.observation_mode(\n",
    "    observation=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "The first element of the result shows the outputs in the form of an xarray, this is where the simulated profiles are stored - we want the signal value in electrons hence we will select the 'pixel' data variable for each signal level used and save them for the calibration to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[0][\"pixel\"].plot(hue=\"filename_id\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(13):\n",
    "    np.savetxt(\n",
    "        target_filenames[i], result[0][\"pixel\"].sel(filename_id=i).squeeze().values\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Calibration simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "### How calibration mode works"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "#### Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "Initially a random set of values are asigned to the arguments that are to be optimised, these values are then used to simulate the desired detector effect on the input image, producing a simulated iamge. This simulated image is then compared to the target image with the fitness value (Pyxels goodness of fit term) based upon the residual values between the simulated and target image. Once a fitness value is given the values of the arguments are mutated using a genetic algorithm approach and simulated again in the hope of getting a lower fitness value. As the parameter space is explored the calibration modes fitness value converges to a minima. The arguemnt values that give this fitness minima are the calibrations modes answer to the values of the particular detector effect seen in the target image."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29",
   "metadata": {},
   "source": [
    "#### Effect of calibration parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "The calibration has multiple parameters that can change the size and computational resources used for the simulation. \n",
    "\n",
    "Population size dictates the number of individual pipelines are run at a time. All individuals within a given population are known as a generation, once all individuals within a generation have been simulated their fitness values are compared, with the best being mutated and used in the following generation, with this number being dictated by the best_decisions parameter. Once the set number of generations is reached, that marks the end of a single evolution. The lowest fitness value found once the given number of evolutions has been reached will be the best answer found by the calibration.\n",
    "\n",
    "It is also possible to parallelise calibration mode with the use of the island parameter, each island is run seperately and at the end of an evolution its best fitness values are compared to the best fitness values found by the other islands, the resulting best arguemnts are then taken into the proceeding evolutions for each island and mutated. It is a way of investigating the parameter space far quicker.\n",
    "\n",
    "If you wish to run a large number of islands it is recommended for optimal use of computation resources to use one thread per worker:\n",
    "\n",
    "\n",
    "`client = Client(threads_per_worker=1)`\n",
    "\n",
    "This way an entire thread can be dedicated to a single island, be careful though as the number of islands will be limited by the number of threads within the CPU of the machine you are using. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31",
   "metadata": {},
   "source": [
    "The following figures will help explain how each calibration parameter works within the calibration mode. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "##### Calibration parameter figures"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "The figure below shows what an individual looks like, it is a single exposure pipeline run that applies a set of trap parameters to the input data, this then produces an image with synthetically induced CTI. This image is then compared to the target data with the residuals of which are used to find a fitness value which is used to determine the accuracy of the trap parameters used for this specific pipeline run."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34",
   "metadata": {},
   "source": [
    "![Calibration](images/Pipeline_calibration.png) Figure 2 - Example of an 'individual'; a single pipeline run by pyxel."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35",
   "metadata": {},
   "source": [
    "Below is the next stage in the calibration mode, it defines most of the parameters used to simulate the calibration. $I_{n}$ represents the island number, and $tp_{n}$ the trap properties f a given individual within a population $P_{n}$ and a given generation $G_{n}$ each individual with have a fitness value associated with it $F_{n}$ with the trap parameters from the best fitness values of the generation being mutated to form the new set of trap parameters $tp_{n}^{*}$. After the defined number of generation is reache the parameters that have the best fitness is the champion for the given evolution. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36",
   "metadata": {},
   "source": [
    "![Calibration](images/Evolution_calibration.png) Figure 3 - Example of an entire evolution."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37",
   "metadata": {},
   "source": [
    "Evolutions can also be chained in series and in parallel (if enough computation threads are present in the machine). The champion fitness values of evolution$_{n}$ are compared between islands to explore the parameter space quicker and more evolutions are run afterwards with mutated versions of the champion parameters found from this comparison. Once the given number of evolutions are complete the final trap parameters found to have the best fitness value is the champion parameter of the entire calibration simulation. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "![Calibration](images/Full_calibration_simulation.png) Figure 4 - Overview of the entire calibration simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39",
   "metadata": {},
   "source": [
    "### Calibration simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40",
   "metadata": {},
   "source": [
    "Similar to the observation mode above, the yaml file has to be loaded and instanciated before the calibration can be run. Once loaded a timestamp is created and this is the directory under which the resulting simulation datasets will be saved. A handy tip is to make sure the directory stated in the yaml file is clear so you know where the data is saved. If you want to know the directory the simulation saves to specifically you can timestamp just before the yaml file is laoded and this should correlate closely with the name of the directory."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41",
   "metadata": {},
   "source": [
    "To start, an exmaple is given that uses relatively little computation resources, simulate these first and then play around with the calibration parameters to get a feel for how long a simulation takes and how well optimised the parameters can become."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "Load yaml file and instinciate the detector, pipeline and calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"yaml_files/cdm_calibration.yaml\")\n",
    "\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline\n",
    "calibration = config.calibration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44",
   "metadata": {},
   "source": [
    "The parameters given in the yaml file are loaded but can also be overwritten if needed. Below is an example of how to overwrte the calibration parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45",
   "metadata": {},
   "outputs": [],
   "source": [
    "config.calibration.algorithm.population_size = 20\n",
    "config.calibration.algorithm.generations = 20\n",
    "config.calibration.num_best_decisions = 5\n",
    "config.calibration.num_evolutions = 2\n",
    "config.calibration.num_islands = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46",
   "metadata": {},
   "source": [
    "The cell below runs the main calibration simulation. First the islands are set up, once this is completed the main calibration simulation is run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "result = pyxel.run_mode(\n",
    "    calibration,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Data analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49",
   "metadata": {},
   "outputs": [],
   "source": [
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50",
   "metadata": {},
   "source": [
    "The result of the calibration takes the form of an xarray dataset. It can be manipulated as the user sees fit and can be analysed in a number of ways. A few examples of analysis tools that pyxel has is shown below"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51",
   "metadata": {},
   "source": [
    "#### Display evolution"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52",
   "metadata": {},
   "source": [
    "Dispaly_evolution provides a graph of the fitness value against the number of evolutions. The island and param_id (which in this case selects a specific charge profile from the list of charge profiles used) can be changed to view each combination of island and parameter simluated. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53",
   "metadata": {},
   "outputs": [],
   "source": [
    "result.load()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_evolution(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55",
   "metadata": {},
   "source": [
    "#### Optimal parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56",
   "metadata": {},
   "source": [
    "The optimal parameters show the results that give the best fitness value from the entire calibration. It also shows which island and evolution gave that result. \n",
    "\n",
    "The champion parameters are in the order specified within the yaml file, for example in the yaml file used in this case stated the two trap release times first and then the two trap densites. So for the table show below, the champion param column shows the following:\n",
    "    \n",
    "row 1 - release timescae for trap 1\n",
    "\n",
    "row 2 - release timescale for trap 2\n",
    "\n",
    "row 3 - trap density for trap 1\n",
    "\n",
    "row 4 - trap density for trap 2 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57",
   "metadata": {},
   "outputs": [],
   "source": [
    "optimal_parameters(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58",
   "metadata": {},
   "source": [
    "#### Champion heatmap "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "59",
   "metadata": {},
   "source": [
    "The champion heatmap shows all of the results saved (the number of best decisions also dictates the number of individuals saved as part of the final xarray result). The plot shows the trap parameters against fitness. The trap parameters can be altered such that only trap release times or densities can be shown for example. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60",
   "metadata": {},
   "outputs": [],
   "source": [
    "champion_heatmap(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61",
   "metadata": {},
   "source": [
    "#### Simulated data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62",
   "metadata": {},
   "source": [
    "Simulated data shows simulated, target profiles and plots them. The residual values can also be viewed and the best result from each island and singal level can be selected to be plotted. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_simulated(result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64",
   "metadata": {},
   "outputs": [],
   "source": [
    "client.shutdown()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "65",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
