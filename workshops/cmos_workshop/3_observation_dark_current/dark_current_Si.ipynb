{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Dark current versus temperature for silicon detectors\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Constanze Seibert\n",
    "\n",
    "## Learning Goals\n",
    "In this notebook we will see the dark current against temperature for silicon (Si) detectors with the influence of two different **noise sources** (at different readout times):\n",
    "- Spatial noise: Fixed-pattern noise (FPN)\n",
    "- Temporal noise: Shot noise\n",
    "\n",
    "## Keywords\n",
    "dark current, observation mode, temperature, silicon detector\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in observation mode by changing the following parameters:\n",
    "1. Temperature from detector operating temperature up to room temperature in K\n",
    "2. Dark current at room temperature = dark current figure of merit at 300 K between 0.01 and 1000 nA/cm^2\n",
    "3. Dark current FPN factor is typically between 0.1 and 0.4, while with FPN=0 no spatial noise is included,\n",
    "\n",
    "and comparing the result in Pyxel with two approximations from datasheets.\n",
    "\n",
    "The dark current model used in Pyxel is adapted from this book:\n",
    "[\"Scientific Charge-Couple Devices.\" by Janesick, J. (2001)](https://spie.org/Publications/Book/374903?SSO=1).\n",
    "Typical values are taken from this paper:\n",
    "[\"High-level numerical simulations of noise in CCD and CMOS photosensors\" by Konnik, M. and Welsh, J. (2014)](https://arxiv.org/abs/1412.4031).\n",
    "Check the dark current model description in the [Pyxel Documentation](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#dark-current)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "# Observation mode\n",
    "\n",
    "<img style=\"float: center;\" src=\"images/observation.png\" width=\"300\">\n",
    "\n",
    "In observation mode, the pipeline is run multiple times with different parameters, specified by the user in the configuration file. We use the library `Xarray`  to save output data for each parameter in a dataset. User can change model parameters as well as detector parameters. Parameters can be specified in the configuration file or loaded from file. The library `matplotlib` is used for visualisation of the results.\n",
    "\n",
    "By the end of this notebook you will know how to:\n",
    "* Run observation mode\n",
    "* Do changes in the configuration file\n",
    "* Inspect and save the observation results\n",
    "\n",
    "\n",
    "**Read more** about the observation mode in the [Pyxel Documentation](https://esa.gitlab.io/pyxel/doc/latest/background/running_modes/observation_mode.html) or about the libraries\n",
    "[Xarray](http://xarray.pydata.org/en/stable/),\n",
    "[matplotlib](https://matplotlib.org/stable/index.html)\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Tip:</b> Open the <i>observation.yaml</i> file in parallel for a better overview.</div>\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pyxel\n",
    "import xarray as xr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.__version__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Create a Dask Cluster and Client"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "from distributed import Client, LocalCluster\n",
    "\n",
    "cluster = LocalCluster(processes=True)\n",
    "client = Client(cluster)\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "## Loading the configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a configuration class from the yaml file\n",
    "config = pyxel.load(\"observation.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "## Running the pipelines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "# Plotting the outputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define dark current from pixel dataset\n",
    "dark_current = result[\"/bucket/pixel\"].mean(dim=[\"x\", \"y\"])\n",
    "temperature = result[\"/bucket\"].temperature\n",
    "\n",
    "# dark current without spatial noise\n",
    "dark_current_no_fpn = (\n",
    "    result[\"/bucket/pixel\"].sel(spatial_noise_factor=0.0).mean(dim=[\"x\", \"y\"])\n",
    ")\n",
    "# dark current with pattern noise and figure of merit=0.01\n",
    "dark_current_fpn = result[\"/bucket/pixel\"].sel(figure_of_merit=1).mean(dim=[\"x\", \"y\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "## Different dark current figure of merit values\n",
    "Plot 1 will show the dark current model used in Pyxel against the temperature for different figure of merit values. It is the dark current figure of merit at 300 K in nA/cm^2. \n",
    "\n",
    "- A higher value for the figure of merit means that the dark current is higher and it will reach the full-well capacity (FWC) already at lower temperatures in comparison to a small figure of merit value.\n",
    "- FPN is set to 0, so no spatial noise is included."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(len(result[\"/bucket/figure_of_merit\"])):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current_no_fpn.isel(figure_of_merit=i),\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"{result['/bucket/figure_of_merit'].to_numpy()[i]}\"\n",
    "        r\" $\\mathrm{nA/cm^2}$\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature for different figure of merit\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "### Comparison of result from Pyxel simnulation to literature\n",
    "Below is a figure from the book [\"Scientific Charge-Couple Devices.\" by Janesick, J. (2001)](https://spie.org/Publications/Book/374903?SSO=1), p.625. It shows the same plot like above, but the unit of the tempeature is Celsius instead of Kelvin and without FWC."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "<img style=\"float: center;\" src=\"images/Dc_T_fom.png\" width=\"300\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "## Different fixed-pattern noise factors\n",
    "Plot 2 will show the dark current model used in Pyxel against the temperature for different FPN values. \n",
    "    \n",
    "- The dark current at 300 K is 0.01 nA/cm^2. \n",
    "- Only at high temperatures (or for long exposure times) the FPN plays a role. \n",
    "- Higher values result in higher dark current values and an earlier saturation of the pixel to the FWC.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Try:</b> You can set temporal noise to <b>true</b> in the <i>yaml</i> file, run everything again and see the difference in the plots, if temporal noise is included. </div>\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(2)\n",
    "for i in range(len(result[\"/bucket/spatial_noise_factor\"])):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current_fpn.isel(spatial_noise_factor=i),\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"FPN: {result['/bucket/spatial_noise_factor'].to_numpy()[i]}\",\n",
    "    )\n",
    "plt.title(\n",
    "    \"Dark current vs. temperature for different fixed pattern noise (FPN) factors\"\n",
    ")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "## Comparison of result with two different approximations\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "**Approx1**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "# between 230 and 300 K valid\n",
    "\n",
    "\n",
    "def dark_current_approx1(result):\n",
    "    T = np.arange(230.0, 300.0, 1.0)\n",
    "    Dc293 = 250  # e/pixel/s\n",
    "    Dc = (1.14e06 * T**3 * np.exp(-9080 / T)) * Dc293\n",
    "\n",
    "    return Dc, T"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "**Approx2**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "# between 230 and 300 K valid\n",
    "\n",
    "\n",
    "def dark_current_approx2(result):\n",
    "    T = np.arange(230.0, 300.0, 1.0)\n",
    "    Dc293 = 20000  # e/pixel/s\n",
    "    Dc = (122 * T**3 * np.exp(-6400 / T)) * Dc293\n",
    "\n",
    "    return Dc, T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "Dc_approx1, T = dark_current_approx1(result)\n",
    "Dc_approx2, T = dark_current_approx2(result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(len(result[\"/bucket/figure_of_merit\"])):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current_no_fpn.isel(figure_of_merit=i),\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"{result['/bucket/figure_of_merit'].to_numpy()[i]}\"\n",
    "        r\" $\\mathrm{nA/cm^2}$\",\n",
    "    )\n",
    "plt.semilogy(T, Dc_approx1, \"-\", color=\"brown\", ms=5, mfc=\"none\", label=\"Approx 1\")\n",
    "plt.semilogy(T, Dc_approx2, \"-\", color=\"black\", ms=5, mfc=\"none\", label=\"Approx 2\")\n",
    "plt.semilogy(293, 250, \"*\", color=\"brown\", label=\"Typical value for Approx 1\")\n",
    "plt.semilogy(\n",
    "    293,\n",
    "    20000,\n",
    "    \"*\",\n",
    "    color=\"black\",\n",
    "    label=\"Typical value for Approx 2\",\n",
    ")\n",
    "plt.title(\n",
    "    \"Dark current vs. temperature for different figure of merit values in comparison to two approximations\"\n",
    ")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend(loc=\"center left\", bbox_to_anchor=(1, 0.5));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "## Plotting image as function of readout time, temperature and figure of merit\n",
    "\n",
    "Library `holoviews` can be used to quickly plot data stored inside `xarray` datasets.\n",
    "Here with the `bokeh` backend, `matplotlib` is also supported.\n",
    "\n",
    "You can use the toolbar to change temperature and figure of merit to see the influence on the detector.\n",
    "\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Try:</b> You can change the <b>readout time</b> in the <i>yaml</i> file, reload the configuration and run the pipeline again. Then display the results and see the influence of the new readout time.</div>\n",
    "\n",
    "Longer readout times result in higher dark current and saturation is reached at lower temperatures already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "def display_array(data: xr.DataArray, color_map=\"gray\", num_bins=100) -> \"Layout\":\n",
    "    \"\"\"Display detector interactively.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    detector: Detector\n",
    "    hist: bool\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    hv.Layout\n",
    "        A Holoviews object.\n",
    "    \"\"\"\n",
    "\n",
    "    # Late import to speedup start-up time\n",
    "    import holoviews as hv\n",
    "\n",
    "    # Apply an extension to Holoviews (if needed)\n",
    "    if not hv.Store.renderers:\n",
    "        hv.extension(\"bokeh\")\n",
    "\n",
    "    assert \"y\" in data.dims\n",
    "    assert \"x\" in data.dims\n",
    "\n",
    "    other_dims = [name for name in data.dims if name not in (\"y\", \"x\")]\n",
    "\n",
    "    dct = {key: data[key].values for key in other_dims}\n",
    "\n",
    "    min_value = float(data.min())\n",
    "    max_value = float(data.max())\n",
    "\n",
    "    def _get_image(*params: tuple):\n",
    "        selected_coords = dict(zip(other_dims, params))\n",
    "        selected_data = data.sel(**selected_coords)\n",
    "\n",
    "        num_y, num_x = selected_data.shape\n",
    "\n",
    "        return hv.Image(selected_data).opts(\n",
    "            # clim=(min_value, max_value),\n",
    "            colorbar=True,\n",
    "            cmap=color_map,\n",
    "            tools=[\"hover\"],\n",
    "            aspect=num_y / num_x,\n",
    "            invert_yaxis=True,\n",
    "        )\n",
    "\n",
    "    dmap = (\n",
    "        hv.DynamicMap(_get_image, kdims=other_dims)\n",
    "        .redim.values(**dct)\n",
    "        .opts(framewise=True)\n",
    "    )\n",
    "\n",
    "    hist = dmap.hist(adjoin=False, num_bins=num_bins).opts(\n",
    "        aspect=1.5, framewise=True, tools=[\"hover\"], xlabel=\"z\"\n",
    "    )\n",
    "\n",
    "    out = (dmap.relabel(\"Array\") + hist.relabel(\"Histogram\")).opts(tabs=True)\n",
    "\n",
    "    return out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_array(result[\"/bucket/pixel\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc-autonumbering": true,
  "toc-showcode": true,
  "toc-showmarkdowntxt": true
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
