{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Simple non-linearity and ADC saturation modelisation\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../../images/pyxel_logo.png\" width=\"250\">\n",
    "                                                                     \n",
    "Authors\n",
    "-------\n",
    "Benoît Serra, Fiorenzo Stoppa\n",
    "\n",
    "Keywords\n",
    "--------\n",
    "\n",
    "Non-linearity, Models, Simple\n",
    "\n",
    "Prerequisites\n",
    "-------------\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`intro_install_pyxel`  | Necessary | Background |\n",
    "| [xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "\n",
    "Learning Goals\n",
    "--------------\n",
    "By the end of the workshop you will know how to:\n",
    "* Load a configuration file\n",
    "* Run Pyxel in observation mode from the Jupyter notebook\n",
    "* Display/access the final detector object\n",
    "* Create a module for non-linear response of detector\n",
    "* Create a data_processing module to find non-linear behaviour\n"
   ]
  },
  {
   "cell_type": "raw",
   "id": "1",
   "metadata": {},
   "source": [
    "# Download 'exposure.yaml' from https://gitlab.com/esa/pyxel-data/-/raw/master/workshops/pyxel_workshop_as24/simple_nonlinearity_observation.yaml?ref_type=heads&inline=false\n",
    "%pip install pyxel-sim"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pyxel\n",
    "from pyxel.detectors import CMOS\n",
    "\n",
    "print(pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "# Methods cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dedicated cell for functions\n",
    "\n",
    "\n",
    "def simple_nonlinearity(detector, **kwargs): ...\n",
    "\n",
    "\n",
    "def detect_saturation(detector, **kwargs): ..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "# Modelisation of non-linear response of the detector\n",
    "\n",
    "The pixel of a detector has a limit as to how many charges can be photo-generated: this is the **full well capacity** of the detector. As the pixel non-depleted area shrinks, the capacitance of the pixel decrease, changing the gain of the pixel over accumulation of signal.\n",
    "This manifests by non-linearity of the pixel response with increasing signal.\n",
    "\n",
    "**Simple modelisation**: loosing some signal at the charge generation level depending on how close to the full well we are.\n",
    "\n",
    "The usual way to create a model is to create it in your pyxel installation folder via the use of the create-model command:\n",
    "\n",
    "`!python -m pyxel create-model simple_nonlinearity` or `python -m pyxel create-model`\n",
    "\n",
    "For this workshop, we will use a workaround that allows users to create model directly in the notebook that makes it easier.\n",
    "\n",
    "Input parameters\n",
    "----------------\n",
    "\n",
    "- a CMOS detector (pyxel.detectors.CMOS)\n",
    "- amplitude for the non-linear behaviour (float)\n",
    "\n",
    "Introduce a non-linear behavior\n",
    "-------------------------------\n",
    "\n",
    "- extract detector/warm electronics characteristics (present in YAML configuration)\n",
    "    - full_well_capacity\n",
    "    - adc_bit_resolution\n",
    "    - adc_voltage_range\n",
    "- Compute the ADC resolution that is necessary to express the full well capacity as a voltage\n",
    "    - $B = ADC_{range} [V] / ADC_{max ADU}$\n",
    "    - Full well value is multiplied by B\n",
    "- Modify the signal array so that it looses more signal the closer it is to the full well capacity of the detector\n",
    "    - Formula to use is $DET_{signal}=DET_{signal} \\times (1 - A \\times e^{DET_{signal}/FW})$\n",
    "        - $DET_{signal}$ is the signal array of the detector object\n",
    "        - $A$ is the amplitude that we apply to the non-linearity\n",
    "        - $FW$ is the full well in the properties of the detector"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "## Create the simple non-linearity model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "<span style='color:red'>**Your method goes to the method cell in the beginning.**</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "## Change the pipelines\n",
    "Now, this model needs to be used in the pipelines when running the exposure, for that, you need to modify the yaml configuration file to call the previously written function:\n",
    " - Open `simple_nonlinearity.yaml`\n",
    " - Fill the `name`, `func`, `enabled`, and `arguments` parameters\n",
    "    - `arguments` need to have the `amplitude` needed for this model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "These are the lines that should appear in the `charge_measurement` entry of the YAML file:\n",
    "```yaml\n",
    "    - name: as24_nonlinearity_example\n",
    "      func: __main__.simple_nonlinearity\n",
    "      enabled: true\n",
    "      arguments:\n",
    "        amplitude: 0.0\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "## Execute pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "Back to the basics of pyxel, use the load function to load the `exposure`, `detector` and `pipeline` configurations. In another cell, run the exposure and display the XArray resulting from the exposure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"simple_nonlinearity_observation.yaml\")\n",
    "\n",
    "mode = config.running_mode\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "result_ds = pyxel.run_mode(\n",
    "    mode=mode,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    "    with_inherited_coords=True,\n",
    ")\n",
    "\n",
    "result_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "## Plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "ax.set_title(\"Simulation of non-linear behavior with simple non-linearity model\")\n",
    "ax.set_xlabel(\"Integration time [AU]\")\n",
    "ax.set_ylabel(\"Pixel [ADU]\")\n",
    "result_ds[\"image\"].isel(y=40, x=32).plot(hue=\"amplitude\", marker=\".\", ax=ax)\n",
    "plt.grid(True, alpha=0.5)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "# Test for ADC saturation\n",
    "\n",
    "In the pipelines there is the `simple_adc` that encodes the data in 16 bits in a 5V range. Meaning that any signal over 5V will result in an image at 65,535. While the pipelines are running in observation mode, this model will tell us whether we reach ADC saturation and at which time, so that we can change either the integration time or the incoming flux.\n",
    "\n",
    "Input Parameters:\n",
    "-----------------\n",
    "*   A threshold percentile for saturation level\n",
    "\n",
    "Search for non-linearity:\n",
    "-------------------------\n",
    "*   Access the detector.characteristics.adc_bit_resolution and calculate the max value possible.\n",
    "*   At each time (detector.absolute_time) check if the max value is reached.\n",
    "    *   Create an xr.Dataset()\n",
    "    *   Define 'saturation' and its units 'adu' \n",
    "    *   Define 'time' and its units 's'\n",
    "    *   Add the to detector.data as a DataTree the xr created above (only if the array was not already there)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "## Create a model to detect ADC saturation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "<span style='color:red'>**Your method goes to the method cell in the beginning.**</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "## Change the pipelines\n",
    "\n",
    "Modify once again the yaml configuration file ('simple_nonlinearity') to add the created module to the data_processing bucket: "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "### SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "These are the lines that should appear in the `data_processing` entry of the YAML file:\n",
    "\n",
    "```yaml\n",
    "    - name: as24_detect_saturation\n",
    "      func: __main__.detect_saturation\n",
    "      enabled: true\n",
    "      arguments:\n",
    "        threshold_percentile: 95.0\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "## Execute the pipeline again "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"simple_nonlinearity_observation.yaml\")\n",
    "\n",
    "mode = config.running_mode\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "result_ds = pyxel.run_mode(\n",
    "    mode=mode,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result_ds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "result_ds[\"/data/detect_saturation\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
