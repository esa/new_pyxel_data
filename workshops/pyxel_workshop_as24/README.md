# 🇯🇵Pyxel workshop 2024 at SPIE
*Workshop at SPIE Astronomical Telescopes + Instrumentation 2024, Yokohama, Japan*

* **DATE:** Thursday, June 20th, 2024
* **TIME:** 10:15AM to 12:15AM Japan Standard Time
* **LOCATION:** Room G216, North, 2F at the Pacifico Yokohama

## PRE-WORKSHOP SETUP

Please ensure your laptop is correctly configured before the workshop by 
following the [installation and setup instructions](https://esa.gitlab.io/pyxel/doc/stable/tutorials/overview.html#quickstart-setup).

As a reminder, you can install Pyxel and its tutorials directly from a Jupyter notebook:

```
%pip install pyxel-sim
!python -m pyxel download-examples
```

As an alternative, the workshop can be run on Google Colab.

*Warning*: Installation and setup may take up to *one hour* depending on 
your current configuration and internet speeds. 
DO NOT WAIT UNTIL THE DAY OF THE WORKSHOP to complete this process.

## Schedule

| Time (JST)     | Topic                                                                                                                                                   | Presenter / Instructor | Description                                                                                          |
|----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|------------------------------------------------------------------------------------------------------|
| 9:15 - 10:15am | Installation and troubleshooting                                                                                                                        | Frederic Lemmel and developer team |                                                                                                      |
| 10:15 - 10:30am | The basics - running my first simulation with Pyxel and general introduction to the exercise                                                            | Frederic Lemmel, Thibaut Prod'homme | Running pyxel basic examples & demonstration, first exercises                                        | 
| 10:30 - 10:45am | FWC measurement and non-linearity exercise part I: adding my model to the pipeline. 15 min demo on how to add a model and exercise part 1 instructions  | Benoit Serra / Frederic Lemmel | Create a new model (this is an example with a link to a Pyxel Data)                                  |
| 10:45-11:15am | Time to solve the exercise part 1 and solution | |
| 11:15 - 11:30am | FWC measurement and non-linearity exercise part II: measuring non linearity and estimating FWC | Fiorenzo Stoppa / Frederic Lemmel | 15 min demo on how to use the data tree in data processing model group, exercise part 2 instructions |
| 11:30 - 12:00am |	Time to solve the exercise part 2 and solution | Fiorenzo Stoppa / Frederic Lemmel |                                                                                                      |
| 12:00am |	Quick wrap-up | Thibaut Prod'homme | instructions for developers  |
