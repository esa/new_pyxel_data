import numpy as np
from pyxel.detectors import CMOS


def simple_nonlinearity(detector: CMOS, amplitude: float) -> None:
    """Operation on the signal array decrease the signal by the exponential
    of the ratio between current signal and full well of the pixel (in volts)
    which introduces a non-linear response of the pixel.

    Parameters
    ----------
    detector : CMOS
        Pyxel detector object.
    amplitude : float
        A factor that scales the non-linearity effect.
        Higher values introduce a stronger non-linear response.
    """
    # Do operation on one of those arrays and return None.
    signal = detector.signal.array

    # The full well capacity is in ADU (or e- if gains are at 1 in yaml file)
    # need to multiply by the ADC resolution (78.125*1e-6)
    full_well = detector.characteristics.full_well_capacity

    # 65536 ADU max for a 16 bits ADC
    adc_max_value = 2**detector.characteristics.adc_bit_resolution

    # ADC range is a list with min max, so a diff will give a the range as a float
    adc_voltage_range = np.diff(detector.characteristics.adc_voltage_range)

    # Full_well in volts
    full_well *= adc_voltage_range / adc_max_value

    # Loss of signal due to non-linearity
    detector.signal.array = signal * (1 - amplitude * np.exp(signal / full_well))

    # Done!


def detect_saturation(detector: CMOS, threshold_percentile: float = 95.0) -> None:
    """Detect if the saturation level is reached in CMOS detector's image.

    This function evaluates if the detector's image has reached a specified saturation level
    based on a given percentile threshold.

    Parameters
    ----------
    detector : CMOS
        Pyxel detector object.
    threshold_percentile: float
        Percentile threshold to determine saturation.
    """
    max_value = 2**detector.characteristics.adc_bit_resolution - 1
    value = np.percentile(detector.image.array, threshold_percentile)

    if value >= max_value:
        if "detect_saturation" not in detector.data:
            detector.data["/detect_saturation/saturation"] = value
            detector.data["/detect_saturation/time"] = detector.absolute_time

            # These attributes are optional and only used by xarray for plotting
            detector.data["/detect_saturation/saturation"].attrs = {"units": "adu"}
            detector.data["/detect_saturation/time"].attrs = {"units": "s"}

    if detector.is_last_readout:
        if "detect_saturation" not in detector.data:
            # If it the last readout and there is no saturation then
            # force saturation to NaN (Not a Number)
            detector.data["/detect_saturation/saturation"] = np.nan
            detector.data["/detect_saturation/time"] = np.nan

            # These attributes are optional and only used by xarray for plotting
            detector.data["/detect_saturation/saturation"].attrs = {"units": "adu"}
            detector.data["/detect_saturation/time"].attrs = {"units": "s"}
