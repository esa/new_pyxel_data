# 🇳🇱 Developer Workshop 2024 at ESTEC
*Developer Workshop at ESA/ESTEC, Noordwijk, The Netherlands*


* **DATE:** Thursday, October 17th, 2024
* **TIME:** 14:00 to 17:30 CEST, Central European Summer Time
* **LOCATION:** Room Ef008 at ESTEC

and 

* **DATE:** Friday, October 18th, 2024
* **TIME:** 10:00 to 14:30 CEST, Central European Summer Time
* **LOCATION:** Room AF205, at ESTEC

## PRE-WORKSHOP SETUP

Please ensure your laptop is correctly configured before the workshop by 
following the [installation and setup instructions](https://esa.gitlab.io/pyxel/doc/stable/tutorials/overview.html#quickstart-setup).

### Installation from a terminal

To quickly set up and use Pyxel for this workshop from the terminal / command line, follow the recommended steps
using [**uv**](https://docs.astral.sh/uv/), a fast Python package and projet manager.

1. **Install uv**: Use one of the following standalone installer from a **terminal**:
      * for macOS, Linux: ```curl -LsSf https://astral.sh/uv/install.sh | sh```
      * or for Windows: ```powershell -c "irm https://astral.sh/uv/install.ps1 | iex"```
   After installation [**uv**](https://docs.astral.sh/uv/), please **open a new terminal**.
3. **Verify Installation**: To check if you can install Pyxel with [**uv**](https://docs.astral.sh/uv/), 
   run the following command from a terminal: 
   ```bash
   uvx pyxel-sim --version
   ```
4. **Download the Tutorial Notebooks**: Download the materials for this workshop with the command: 
   ```bash
   uvx pyxel-sim download-examples
   ```
5. **Run Pyxel with Jupyter Lab**: Start Jupyter Lab (preferred way) and run Pyxel with the following command:
   ```bash
   cd pyxel-examples
   uvx --with pyxel-sim jupyter lab
   ```
6. **Open the notebooks**: In Jupyter Lab, you will find the notebooks for this workshop under
the folder `workshops` and `developer_workshop_2024`. See the following screenshot: ![workshop_folder](images/workshop_folder.png)

   Alternatively, you can also start Pyxel in different environment these commands:
     * With a specified Python version: ```uvx --python 3.11 --with pyxel-sim jupyter lab```
     * With a specified Pyxel version: ```uvx --with "pyxel-sim==2.6" jupyter lab```
     * With ipython: ```uvx --with pyxel-sim ipython```
     * With extra packages: ```uvx --with pyxel-sim --with dask-labextension --with ccdproc jupyterlab```

### Installation from a running Jupyter notebook

As a reminder, you can install Pyxel and its tutorials directly from a running Jupyter notebook:

```python
%pip install pyxel-sim
!python -m pyxel download-examples
```

As an alternative, the workshop can be run on Google Colab.

*Warning*: Installation and setup may take up to *one hour* depending on 
your current configuration and internet speeds. 
**DO NOT WAIT UNTIL THE DAY OF THE WORKSHOP to complete this process.

## Schedule


### Day 1: Thursday, October 17th, 2024
* **Time (CEST): 14:00 - 17:30 CEST**
* **Location: Room Ef008 at ESTEC**

| Time (CEST)   | Topic                                                                                              | Presenter / Instructor | Description                                                                    |
|---------------|----------------------------------------------------------------------------------------------------|------------------------|--------------------------------------------------------------------------------|
| 14:00 - 14:15 | Installation and troubleshooting                                                                   | Frederic Lemmel and developer team |                                                                    |
| 14:15 - 15:00 | The basics - running my first simulation with Pyxel and general introduction to the exercise       | Frederic Lemmel, Thibaut Prod'homme | Running pyxel basic examples & demonstration, first exercises     |
| 15:00 - 17:30 | Photon Transfer Curve exercises in Exposure and Observation mode                                   | Frederic Lemmel and developer team | Run the tutorials                                                  |
| 17:30         | Quick wrap-up | Thibaut Prod'homme | instructions for developers |
| evening       | Dinner in Leiden, The Netherlands in the evening | |

### Day 2: Friday, October 18th, 2024
* **Time (CEST): 10:00 - 14:30 CEST**
* **Location: Room AF205 at ESTEC**

| Time (CEST    | Topic                                                                                              | Presenter / Instructor | Description                                                                    |
|---------------|----------------------------------------------------------------------------------------------------|------------------------|--------------------------------------------------------------------------------|
| 10:00 - 10:30 | Developer meeting                                                                                  | Developer team         |       |
| 10:30 - 14:30 | Hackathon: Dive into hands-on work with your own model(s) or project(s)                            | Developer team         |       | 
