{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Photon Transfer Curve (PTC) with Exposure mode and **destructive** mode\n",
    "\n",
    "<img style=\"float: right;\" src=\"../../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "## Authors\n",
    "[The Pyxel development team](mailto:pyxel@esa.int)\n",
    "\n",
    "## Keywords\n",
    "Exposure mode, Photon Transfer Curve, Destructive readout mode\n",
    "\n",
    "## Prerequisites\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`intro_install_pyxel`  | Necessary | Background |\n",
    "| {ref}`euclid_prnu_ptc` | Helpful | Background |\n",
    "| [Xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [Xarray plotting](https://tutorial.xarray.dev/fundamentals/04.1_basic_plotting.html) | Helpful | Background |\n",
    "\n",
    "\n",
    "## Learning goals\n",
    "By the end of the lesson you will:\n",
    "* Understand and work with models to simulate **Photon Transfer Curve**.\n",
    "* Load and use a Pyxel [YAML configuration file](https://esa.gitlab.io/pyxel/doc/stable/background/yaml.html).\n",
    "* Run Pyxel with **exposure mode** and generate outputs in a [**DataTree**](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree) structure\n",
    "* Use a [Data Processing model](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html).\n",
    "* Identify challenges of destructive readout mode when attempting to create a **Photon Transfer Curve**.\n",
    "\n",
    "## Summary\n",
    "\n",
    "In this notebook, we will try to plot a **Photon Transfer Curve** (PTC) using the **exposure mode** with **destructive readout mode**.\n",
    "\n",
    "It highlights the limitations of destructive readout in generating an effective **Photon Transfer Curve** due to signal reset during readout.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "## What is a Photon Transfer Curve ?\n",
    "\n",
    "A **Photon Transfer Curve** plot is a useful tool in the characterization of image sensors likes CCDs or CMOS.\n",
    "\n",
    "Example of a **Photon Transfer Curve** plot with at the x-axis the **signal level** and y-axis the **noise level**. The **signal level** can be defined in ADUs, electrons, photons... .\n",
    "\n",
    "\n",
    "![ptc_exposure](images/ptc.png)\n",
    "\n",
    "**Photon Transfer Curve** from the 'Photon Transfer' from Janesick\n",
    "\n",
    "### The four noise regimes\n",
    "\n",
    "A **Photon Transfer Curve (PTC)** *can be* characterized by the following regimes:\n",
    "\n",
    "1. **Read Noise Regime** (slope = 0): This regime occurs in **total darkness or low illumination**. It is characterized\n",
    "by **random noise<**, which includes contributions such as thermal noise and dark current.\n",
    "Read noise dominates at very low signal levels.\n",
    "2. **Photon Shot Noise Regime** (slope = ½): As the light levels increase, photon shot noise becomes the dominant form of noise.\n",
    "This regime appears as a linear segment in a log-log plot with a slope of 1/2.\n",
    "Shot noise is a natural consequence of the **random arrival of photons<**, and its magnitude **increases with the\n",
    "square root** of the signal level.\n",
    "3. **Fixed Pattern Noise (FPN) Regime** (slope = 1): At even **higher light levels**, fixed-pattern noise (FPN) emerges.\n",
    "This noises stems from variations in pixel responses and sensor inhomogeneities.\n",
    "In this regime, the **noise scales linearly with the signal**, resulting in a slope of 1 in the PTC.\n",
    "FPN becomes more prominent as the pixel responses begin to diverge due to non-uniformities in the sensor.\n",
    "4. **Full-Well Saturation Regime** (slope = ∞): In the final regime, the subarray of pixels **reaches saturation**,\n",
    "referred to as the full-well regime. Here **noise levels generally decrease** as the pixels become saturated.\n",
    "A sharp deviation in noise from the expected 1/2 or 1 slope signals that the full-well condition has been reached.\n",
    "\n",
    "A **Photon Transfer Curve** is typically represented as a **signal vs variance** plot.\n",
    "\n",
    "\n",
    "You can find more information about 'Photon Transfer' book ([https://doi.org/10.1117/3.725073](https://doi.org/10.1117/3.725073))."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "## Exposure mode\n",
    "\n",
    "#### Destructive readout mode\n",
    "In this tutorial, we **will try** to plot a **Photon Transfer Curve** using the **exposure** mode and **destructive readout mode** (see YAML file):\n",
    "\n",
    "One way to generate a **Photon Transfer Curve** is to **increase the exposure time** to cover a wide range of signals from low illumination to saturation (see YAML snippet below):\n",
    "\n",
    "```yaml\n",
    "exposure:\n",
    "  readout:\n",
    "     times: numpy.arange(start=0.1, stop=10.0, step=0.1)  # <== Increase exposure time from 0.1 to 10.0 by steps of 0.1\n",
    "     non_destructive: false                               # <== DESTRUCTIVE READOUT MODE enabled\n",
    "...\n",
    "```\n",
    "\n",
    "In CCD (Charge-Coupled Device) and CMOS (Complementary Metal-Oxide-Semiconductor) detectors, **destructive readout** mode refers to the process by which the stored charge or signal in each pixel is read out in such a way that it is \"destroyed\" or cleared in the process. \n",
    "\n",
    "This typically means that after reading, the data is no longer available or recoverable for a second readout, unlike non-destructive modes where data can be read multiple times.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pyxel\n",
    "\n",
    "pyxel.__version__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "### Load YAML configuration file\n",
    "\n",
    "Load the YAML configuration file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"ptc_exposure.yaml\")\n",
    "\n",
    "config"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "### Run the pipeline and get a DataTree\n",
    "\n",
    "\n",
    "#### What is a DataTree ?\n",
    "Run the Exposure's pipeline with Pyxel and return an Xarray's [DataTree](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree) object.\n",
    "\n",
    "A [DataTree](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree) is an Xarray high-level data structure, organizing multi-dimensional arrays in a nested tree-like hierarchy.\n",
    "\n",
    "The graph-like structure generated by Pyxel is always the same:\n",
    "```bash\n",
    "/\n",
    "├── bucket      # <=== ALWAYS PRESENT\n",
    "│   ├── photon      : 3D array (time x num_rows x num_cols)\n",
    "│   ├── ...\n",
    "│   └── image       : 3D array (time x num_rows x num_cols)\n",
    "├── scene       # <=== only populated when using a Scene\n",
    "│   ├── 0\n",
    "│   │   └── source  : 3D array (time x num_objects x wavelengths)\n",
    "│   ...\n",
    "└── data        # <=== only populated when using a Data Processing model(s)\n",
    "    ├── statistics\n",
    "    │   └── ...     : nD array\n",
    "    └── mean_variance \n",
    "        └── image\n",
    "            └── ... : 1D array (pipeline_steps)\n",
    "```\n",
    "\n",
    "The node from the graph-like structure can be accessed using a path.\n",
    "Examples:\n",
    "```python\n",
    "data_tree['/bucket/photon']\n",
    "...\n",
    "\n",
    "data_tree['/scene/0/source']\n",
    "...\n",
    "\n",
    "data_tree['/data/mean_variance/image']\n",
    "...\n",
    "```\n",
    "\n",
    "#### Run the exposure pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the Exposure pipeline for multiple readout times\n",
    "data_tree = pyxel.run_mode(\n",
    "    mode=config.running_mode,\n",
    "    detector=config.detector,\n",
    "    pipeline=config.pipeline,\n",
    "    with_inherited_coords=True,  # Provisional, will be set to True by default in future\n",
    ")\n",
    "\n",
    "data_tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display the graph-like structure\n",
    "print(data_tree)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### Access multi-dimensional arrays from the DataTree\n",
    "\n",
    "Access the multi-dimensional **Numpy arrays** `photon`, `charge`, ... stored in the data structure `/bucket` from the output's [DataTree](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access data structure node '/bucket' from the DataTree\n",
    "data_tree[\"/bucket\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "# 3D Numpy array\n",
    "data_tree[\"/bucket/image\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "## 🔥Problem🔥: Cannot do a Photon Transfer Curve with **destructive readout mode**\n",
    "\n",
    "A **Photon Transfer Curve (PTC)** cannot be effectively measured with **destructive readout mode** because the output 'image' is permanently resetted during the readout process after each time's steps.\n",
    "\n",
    "### Plot 'image' in function of 'time'\n",
    "\n",
    "Plot 'image' vs 'time' to demonstrate that the signal is resetted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    data_tree[\"/bucket/image\"]  # Get 'image' (in adu)\n",
    "    .mean(dim=[\"y\", \"x\"], keep_attrs=True)  #  Spatial mean\n",
    "    .plot(marker=\".\")\n",
    ")\n",
    "plt.title(\"Spatial average for 'image'\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "### (Try) to plot PTC generated by model `mean_variance`\n",
    "\n",
    "The **Photon Transfer Curver** can be derived using the model [mean_variance](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#mean-variance) from [Data Processing](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#) group in Pyxel.\n",
    "\n",
    "```yaml\n",
    "...\n",
    "pipeline:\n",
    "  ...\n",
    "  data_processing:\n",
    "    - name: mean_variance\n",
    "      func: pyxel.models.data_processing.mean_variance\n",
    "      enabled: true\n",
    "      arguments:\n",
    "        data_structure: image  # Optional\n",
    "```\n",
    "\n",
    "The processed result will be available in `/data/mean_variance/image` node from the DataTree:\n",
    "```python\n",
    "data_tree['/data/mean_variance/image']\n",
    "...\n",
    "```\n",
    "\n",
    "### ℹ️ Alternative method to get 'mean' and 'variance'\n",
    "\n",
    "Alternatively, you could also compute the mean and variance by averaging the pixel values from 'image' across the spatial dimensions with the following code:\n",
    "\n",
    "```python\n",
    "import xarray as xr\n",
    "\n",
    "ptc = xr.Dataset()\n",
    "ptc[\"mean\"] = data_tree[\"/bucket/image\"].mean(dim=[\"y\", \"x\"])\n",
    "ptc[\"variance\"] = data_tree[\"/bucket/image\"].var(dim=[\"y\", \"x\"])\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1D array\n",
    "data_tree[\"/data/mean_variance/image\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "Attempting to plot a **Photon Transfer Curve** using the [mean_variance](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#mean-variance) model will not succeed because the signal 'image' is continuously reset during the readout process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Attempt to plot PTC using model 'mean_variance'\n",
    "(\n",
    "    data_tree[\"/data/mean_variance/image\"]\n",
    "    .to_dataset()\n",
    "    .plot.scatter(\n",
    "        x=\"mean\",\n",
    "        y=\"variance\",\n",
    "        xlim=[10**0, 10**5],\n",
    "        ylim=[10**0, 10**5],\n",
    "        xscale=\"log\",\n",
    "        yscale=\"log\",\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "## Solution: 😎Use observation mode !😎\n",
    "\n",
    "Use the [Observation mode](https://esa.gitlab.io/pyxel/doc/stable/background/running_modes/observation_mode.html) to set different single readout-time. See the next notebook {ref}`ptc_observation_workshop`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "## Exercise: 💡 Alternative solution with only 'exposure' mode\n",
    "\n",
    "You can still plot a **Photon Transfer Curve** in **exposure** mode by enabling the **non-destructive readout mode**.\n",
    "\n",
    "You can do this by modifing the YAML configuration file and setting the 'non_destructive' to False like this:\n",
    "```yaml\n",
    "exposure:\n",
    "  readout:\n",
    "    times: ...\n",
    "    non_destructive: true   # <== set to NON-DESTRUCTIVE READOUT\n",
    "```\n",
    "\n",
    "or using by overriding `config.exposure.readout.non_destructive` with the following line in Python:\n",
    "```python\n",
    "config.exposure.readout.non_destructive = True\n",
    "```\n",
    "\n",
    "Then, re-run this notebook to observe the changes"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
