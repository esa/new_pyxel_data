{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "(ptc_observation_workshop)=\n",
    "# Photon Transfer Curve with Observation mode\n",
    "\n",
    "<img style=\"float: right;\" src=\"../../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "## Authors\n",
    "[The Pyxel development team](mailto:pyxel@esa.int)\n",
    "\n",
    "## Keywords\n",
    "Observation mode, Photon Transfer Curve, Destructive readout mode\n",
    "\n",
    "## Prerequisites\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`intro_install_pyxel`  | Necessary | Background |\n",
    "| [xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [Xarray plotting](https://tutorial.xarray.dev/fundamentals/04.1_basic_plotting.html) | Helpful | Background |\n",
    "| [Parallel Computing with Dask](https://docs.xarray.dev/en/stable/user-guide/dask.html) | Helpful | Background |\n",
    "\n",
    "## Learning goals\n",
    "By the end of the lesson you will know how to:\n",
    "* Load and use a Pyxel [YAML configuration file](https://esa.gitlab.io/pyxel/doc/stable/background/yaml.html).\n",
    "* Run Pyxel with **observation mode** and generate outputs in a [**DataTree**]((https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree)) structure\n",
    "* Understand and work with models to simulate **Photon Transfer Curve**.\n",
    "* Work with [DataTree](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree).\n",
    "* Work with a [Data Processing model](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html).\n",
    "* Plot and analyze Photon Transfer Curve plot.\n",
    "\n",
    "## Summary\n",
    "\n",
    "In this notebook you will learn to generate and analyze a **Photon Transfer Curve** using **observation mode** and **destructive readout**.\n",
    "\n",
    "The notebook will guide users through simulating **Photon Transfer Curve** and visualizing results effectively."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "## What is a Photon Transfer Curve ?\n",
    "\n",
    "A **Photon Transfer Curve** plot is a useful tool in the characterization of image sensors likes CCDs or CMOS.\n",
    "\n",
    "Example of a **Photon Transfer Curve** plot with at the x-axis the **signal level** and y-axis the **noise level**. The **signal level** can be defined in ADUs, electrons, photons... .\n",
    "\n",
    "\n",
    "![ptc_exposure](images/ptc.png)\n",
    "\n",
    "**Photon Transfer Curve** from the 'Photon Transfer' from Janesick\n",
    "\n",
    "### The four noise regimes\n",
    "\n",
    "A **Photon Transfer Curve (PTC)** *can be* characterized by the following regimes:\n",
    "\n",
    "1. **Read Noise Regime** (slope = 0): This regime occurs in **total darkness or low illumination**. It is characterized\n",
    "by **random noise<**, which includes contributions such as thermal noise and dark current.\n",
    "Read noise dominates at very low signal levels.\n",
    "2. **Photon Shot Noise Regime** (slope = ½): As the light levels increase, photon shot noise becomes the dominant form of noise.\n",
    "This regime appears as a linear segment in a log-log plot with a slope of 1/2.\n",
    "Shot noise is a natural consequence of the **random arrival of photons<**, and its magnitude **increases with the\n",
    "square root** of the signal level.\n",
    "3. **Fixed Pattern Noise (FPN) Regime** (slope = 1): At even **higher light levels**, fixed-pattern noise (FPN) emerges.\n",
    "This noises stems from variations in pixel responses and sensor inhomogeneities.\n",
    "In this regime, the **noise scales linearly with the signal**, resulting in a slope of 1 in the PTC.\n",
    "FPN becomes more prominent as the pixel responses begin to diverge due to non-uniformities in the sensor.\n",
    "4. **Full-Well Saturation Regime** (slope = ∞): In the final regime, the subarray of pixels **reaches saturation**,\n",
    "referred to as the full-well regime. Here **noise levels generally decrease** as the pixels become saturated.\n",
    "A sharp deviation in noise from the expected 1/2 or 1 slope signals that the full-well condition has been reached.\n",
    "\n",
    "A **Photon Transfer Curve** is typically represented as a **signal vs variance** plot.\n",
    "\n",
    "\n",
    "You can find more information about 'Photon Transfer' book ([https://doi.org/10.1117/3.725073](https://doi.org/10.1117/3.725073))."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "## Observation mode\n",
    "\n",
    "#### Destructive readout mode\n",
    "In this tutorial, we **will try** to plot a **Photon Transfer Curve** using the **observation** mode and **destructive readout mode** (see YAML file):\n",
    "```yaml\n",
    "observation:                     # <== Observation mode\n",
    "  readout:\n",
    "     times: [1.0]  # will be overridden\n",
    "     non_destructive: false      # <== DESTRUCTIVE READOUT MODE\n",
    "\n",
    "  parameters:\n",
    "    - key: observation.readout.times             # <=== Modify readout time\n",
    "      values: numpy.geomspace(start=1e-5, stop=0.5, num=100)   # [1e-5, 1.1e-5, ..., 4.5e-1, 5e-1]\n",
    "      \n",
    "    - key: detector.environment.temperature\n",
    "      values: numpy.linspace(40, 200, num=2)  # [40., 200.]\n",
    "    ...     \n",
    "...\n",
    "```\n",
    "\n",
    "#### Models used in Pyxel for the Photon Transfer Curve\n",
    "\n",
    "The following Pyxel's models can be used to change the **Photon Transfer Curve**.\n",
    "* **Read noise**: [pyxel.models.charge_generation.dark_current_rule07](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/charge_generation_models.html#dark-current-rule07)\n",
    "* **Photon shot noise**: [pyxel.models.charge_measurement.output_node_noise](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/charge_measurement_models.html#output-node-noise)\n",
    "* **Fixed pattern noise (FPN)**: [pyxel.models.charge_collection.fixed_pattern_noise_factor](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/charge_collection_models.html#fixed-pattern-noise)\n",
    "* **Full well capacity**: [pyxel.models.charge_collection.simple_full_well](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/charge_collection_models.html#simple-full-well)\n",
    "\n",
    "Moreover the **Photon Transfer Curve** can be retrieved with the model [mean_variance](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#mean-variance) from [Data Processing](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pyxel\n",
    "\n",
    "pyxel.__version__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Create a Local Dask Cluster (optional and advanced)\n",
    "\n",
    "You can use a Local Dask cluster.\n",
    "\n",
    "While not strictly necessary, the dashboard provides a nice learning tool.\n",
    "\n",
    "See the following picture:\n",
    "![dask dashboard](images/dask_client.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Optional\n",
    "from dask.distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "### Load YAML configuration file\n",
    "\n",
    "Load the Observation YAML configuration file with the 'dask' mode enabled and 'destructive' mode:\n",
    "\n",
    "```yaml\n",
    "observation:\n",
    "\n",
    "  with_dask: true                # <== Dask enabled\n",
    "  readout:\n",
    "     times: [1.0]  # will be overridden\n",
    "     non_destructive: false      # <== DESTRUCTIVE MODE ENABLED !\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"ptc_observation.yaml\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "config.observation.with_dask"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### Run the pipelines with lazy computation using 'dask'\n",
    "\n",
    "When enabling 'dask', the Xarray [**DataTree**](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree) returned by `pyxel.run_mode` wraps the multi-dimensional arrays in **Dask arrays**.\n",
    "\n",
    "This means computations are deferred until explicitly triggered. This is a process known as **lazy computation**.\n",
    "\n",
    "At this point, **no data** is loaded in memory ! This is managed by the [dask](https://www.dask.org).\n",
    "\n",
    "One the data is requested, computations will be **performed in parallel**.\n",
    "\n",
    "For more details, check out the [Parallel computing with dask](https://tutorial.xarray.dev/intermediate/xarray_and_dask.html) tutorial from Xarray."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the Observation running mode\n",
    "# The DataTree is lazily computed and not loaded in memory !\n",
    "data_tree = pyxel.run_mode(\n",
    "    mode=config.running_mode,\n",
    "    detector=config.detector,\n",
    "    pipeline=config.pipeline,\n",
    "    with_inherited_coords=True,\n",
    ")\n",
    "\n",
    "data_tree"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "#### Display the size of the [**DataTree**](https://docs.xarray.dev/en/latest/user-guide/data-structures.html#datatree). \n",
    "\n",
    "Remember, at this point, **no data** is loaded into memory !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "from dask.utils import format_bytes\n",
    "\n",
    "print(\"Size DataTree:\", format_bytes(data_tree.nbytes))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "### Display the node `/bucket/photon` from the DataTree\n",
    "\n",
    "Here, you'll notice additional dimensions like `temperature` or `std_deviation`.\n",
    "\n",
    "The data are also represented using a **Dask Array**, which efficiently the **lazy computation** and the multi-dimensional array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree[\"/bucket/photon\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "### Extracting a 3D array from node '/bucket/photon'\n",
    "\n",
    "To extract the values for the first `temperature`, `full_well_capacity`, `fixed_pattern_noise_factor` and `std_deviation` from node `/bucket/photon`, you can use the Xarray command `.isel(...)`.\n",
    "\n",
    "For more details on how to use `.isel`, check out this Xarray [tutorial](https://tutorial.xarray.dev/fundamentals/02.1_indexing_Basic.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    data_tree[\"/bucket/photon\"].isel(\n",
    "        temperature=0,  # select 'temperature' index 0\n",
    "        full_well_capacity=0,  # select 'full_well_capacity' index 0\n",
    "        fixed_pattern_noise_factor=0,  # select 'fixed_pattern_noise_factor' index 0\n",
    "        std_deviation=0,  # select 'std_deviation' index 0\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "Alternatively, you can also access the same data using the Numpy-style indexing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree[\"/bucket/photon\"][:, 0, 0, 0, 0, :, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "### Plot the spatial mean for `/bucket/photon`\n",
    "\n",
    "You can plot the spatial mean for the first `temperature`, `full_well_capacity`, `fixed_pattern_noise_factor` and `std_deviation` from the node `/bucket/photon` using the following code:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    data_tree[\"/bucket/photon\"]\n",
    "    .isel(  # select first 'temperature', 'full_well_capacity', ...\n",
    "        temperature=0,\n",
    "        full_well_capacity=0,\n",
    "        fixed_pattern_noise_factor=0,\n",
    "        std_deviation=0,\n",
    "    )\n",
    "    .mean(dim=[\"y\", \"x\"], keep_attrs=True)  # spatial mean\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    data_tree[\"/bucket/photon\"]\n",
    "    .isel(  # select first 'temperature', 'full_well_capacity', ...\n",
    "        temperature=0,\n",
    "        full_well_capacity=0,\n",
    "        fixed_pattern_noise_factor=0,\n",
    "        std_deviation=0,\n",
    "    )\n",
    "    .mean(dim=[\"y\", \"x\"], keep_attrs=True)  # spatial mean\n",
    "    .plot.scatter()  # and plot\n",
    ")\n",
    "\n",
    "plt.title(\n",
    "    \"time vs Photon\\n\"\n",
    "    \"temperature = 20, std_deviation=5.45e-5,\\n\"\n",
    "    \"fixed_pattern_noise = 0.014, full_well_capacity=175000\"\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    data_tree[\"/bucket/image\"]\n",
    "    .isel(  # select first 'temperature', 'full_well_capacity', ...\n",
    "        temperature=0,\n",
    "        full_well_capacity=0,\n",
    "        fixed_pattern_noise_factor=0,\n",
    "        std_deviation=0,\n",
    "    )\n",
    "    .mean(dim=[\"y\", \"x\"], keep_attrs=True)\n",
    "    .plot.scatter()\n",
    ")\n",
    "\n",
    "plt.title(\n",
    "    \"time vs image[adu]\\n\"\n",
    "    \"temperature = 20, std_deviation=5.45e-5,\\n\"\n",
    "    \"fixed_pattern_noise = 0.014, full_well_capacity=175000\"\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "## Get the Photon Transfer Curve (PTC) using model `mean_variance`\n",
    "\n",
    "The **Photon Transfer Curver** can be derived using the model [mean_variance](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#mean-variance) from [Data Processing](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/data_processing_models.html#) group in Pyxel.\n",
    "\n",
    "```yaml\n",
    "...\n",
    "pipeline:\n",
    "  ...\n",
    "  data_processing:\n",
    "    - name: mean_variance\n",
    "      func: pyxel.models.data_processing.mean_variance\n",
    "      enabled: true\n",
    "      arguments:\n",
    "        data_structure: image  # Optional\n",
    "\n",
    "```\n",
    "\n",
    "The processed result will be available in `/data/mean_variance/image` node from the DataTree:\n",
    "```python\n",
    "data_tree['/data/mean_variance/image']\n",
    "...\n",
    "```\n",
    "\n",
    "### ℹ️ Alternative method to get 'mean' and 'variance'\n",
    "\n",
    "Alternatively, you could also compute the mean and variance by averaging the pixel values from 'image' across the spatial dimensions with the following code:\n",
    "\n",
    "```python\n",
    "import xarray as xr\n",
    "\n",
    "ptc = xr.Dataset()\n",
    "ptc[\"mean\"] = data_tree[\"/bucket/image\"].mean(dim=[\"y\", \"x\"])\n",
    "ptc[\"variance\"] = data_tree[\"/bucket/image\"].var(dim=[\"y\", \"x\"])\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "### Get multi-dimensional mean-variance data\n",
    "\n",
    "Again, you'll notice additional dimensions like `temperature` or `std_deviation` when extracting the 'mean-variance' data.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "ptc = data_tree[\"/data/mean_variance/image\"].to_dataset()\n",
    "\n",
    "ptc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "from dask.utils import format_bytes\n",
    "\n",
    "print(\"Size DataTree:\", format_bytes(ptc.nbytes))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "### Computation\n",
    "\n",
    "The easiest way to convert an xarray data structure from lazy Dask arrays into eager, in-memory NumPy arrays, you can simply use the `.load()` method.\n",
    "\n",
    "The computation will be done in parallel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "# Warning: this takes ~1 minute\n",
    "ptc.load()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29",
   "metadata": {},
   "source": [
    "### Extract a single **Photon Transfer Curve**\n",
    "\n",
    "Again, to extract a single **Photon Transfer Curve** from the first `temperature`, `full_well_capacity`, `fixed_pattern_noise_factor` and `std_deviation` from node `/bucket/photon`, you can use the Xarray command `.isel(...)` :\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "ptc_subset = ptc.isel(  # get the first\n",
    "    temperature=0,\n",
    "    full_well_capacity=0,\n",
    "    fixed_pattern_noise_factor=0,\n",
    "    std_deviation=0,\n",
    ")\n",
    "\n",
    "ptc_subset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Quick plot\n",
    "ptc_subset.plot.scatter(x=\"mean\", y=\"variance\", xscale=\"log\", yscale=\"log\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyxel.plotting import plot_ptc\n",
    "\n",
    "# Plot PTC in log-log scale and display the four classical noise regimes\n",
    "plot_ptc(ptc_subset)\n",
    "\n",
    "plt.title(\n",
    "    \"PTC with four classical noise regimes\\n\\n\"\n",
    "    \"temperature = 20, std_deviation=5.45e-5,\\n\"\n",
    "    \"fixed_pattern_noise = 0.014, full_well_capacity=175000\"\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "## Plotting with all parameters\n",
    "\n",
    "Plot the Photon Transfer Curve (PTC) plot usign the [`Panel`](https://panel.holoviz.org) library for interactive controls."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "from typing import Any\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import panel as pn\n",
    "import param as pm\n",
    "import xarray as xr\n",
    "from pyxel.plotting import plot_ptc\n",
    "\n",
    "# Initialize 'Panel' extension\n",
    "pn.extension()\n",
    "\n",
    "\n",
    "# Define a class for managing parameters related to PTC\n",
    "class ParametersPTC(pm.Parameterized):\n",
    "    \"\"\"Class for plotting a PTC plot.\"\"\"\n",
    "\n",
    "    @classmethod\n",
    "    def build(cls, dataset: xr.Dataset) -> \"ParametersPTC\":\n",
    "        \"\"\"Create a 'Parameters' class based on 'dataset'.\n",
    "\n",
    "        It dynamically creates 'parameters' based on the coordinates of 'dataset'.\n",
    "        \"\"\"\n",
    "        # Create a new instance\n",
    "        parameters = cls()\n",
    "        parameters._dataset: xr.Dataset = dataset\n",
    "\n",
    "        # Add dynamically new 'parameters'\n",
    "        for coord_name in dataset.coords:\n",
    "            if coord_name in (\"time\", \"readout_time\"):\n",
    "                continue\n",
    "\n",
    "            parameters.param.add_parameter(\n",
    "                coord_name,\n",
    "                pm.Selector(objects=list(dataset[coord_name].to_numpy())),\n",
    "            )\n",
    "\n",
    "        return parameters\n",
    "\n",
    "    def view(self):\n",
    "        \"\"\"Generate interactive view of the PTC plot.\"\"\"\n",
    "        # Create a dictionary of selected coordinates\n",
    "        dct: dict[str, Any] = {\n",
    "            coord_name: getattr(self, coord_name)\n",
    "            for coord_name in self._dataset.coords\n",
    "            if (coord_name not in (\"time\", \"readout_time\"))\n",
    "        }\n",
    "\n",
    "        # Select the new data based on the nearest match of the selected parameters\n",
    "        data: xr.Dataset = self._dataset.sel(dct, method=\"nearest\")\n",
    "\n",
    "        # Create a new Matplotlib PTC plot\n",
    "        fig = plt.Figure(figsize=(3.5, 3.5))\n",
    "        ax = fig.subplots()\n",
    "\n",
    "        plot_ptc(data, ax=ax)\n",
    "\n",
    "        # Return the Matplotlib plot embedded in a Panel pane\n",
    "        return pn.pane.Matplotlib(fig)\n",
    "\n",
    "    def display_params(self):\n",
    "        \"\"\"Display the parameter widgets for user interaction.\"\"\"\n",
    "        # Define 'DiscreteSlider' widgets for each parameter\n",
    "        widgets: dict[str, pn.widgets.Widget] = {\n",
    "            coord_name: pn.widgets.DiscreteSlider for coord_name in self._dataset.coords\n",
    "        }\n",
    "\n",
    "        # Return a panel with parameter widgets\n",
    "        return pn.Param(self.param, widgets=widgets)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters_ptc: ParametersPTC = ParametersPTC.build(ptc)\n",
    "\n",
    "# Create a Panel layout with the PTC plot and parameter widgets\n",
    "pn.Row(\n",
    "    parameters_ptc.view,  # Display the PTC\n",
    "    pn.Column(\n",
    "        parameters_ptc.display_params,\n",
    "        align=\"center\",\n",
    "        width=200,\n",
    "    ),  # Display the parameters\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: Check simple_adc, problem with FWC ?\n",
    "# TODO: warning limit of ADC"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
